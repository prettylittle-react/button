import React from 'react';
import PropTypes from 'prop-types';

import Link from 'link';

import './Button.scss';

class LinkButton extends Link {}

LinkButton.defaultProps = {
	...Link.defaultProps,
	...{
		baseClass: 'btn'
	}
};

LinkButton.propTypes = {
	...Link.propTypes,
	...{}
};

export default LinkButton;
