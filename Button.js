import React from 'react';
import PropTypes from 'prop-types';

//import Text from 'text';
import Icon from 'icon';
import Loader from 'loader';

import {Content, Editable, Heading, Img, RichText, Text} from 'editable';

import {getModifiers} from 'libs/component';

import './Button.scss';

/**
 * Button
 * @description [Description]
 * @example
  <div id="Button"></div>
  <script>
    ReactDOM.render(React.createElement(Components.Button, {
    	title : 'Example Button'
    }), document.getElementById("Button"));
  </script>
 */
class Button extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			isDisabled: props.isDisabled,
			isLoading: false
		};

		this.baseClass = 'btn';
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.isDisabled !== null) {
			this.setState({isDisabled: nextProps.isDisabled, isLoading: nextProps.isLoading});
		}
	}

	get atts() {
		const {onClick, isWide, type, size, style} = this.props;
		const {isDisabled, isLoading} = this.state;

		const atts = {
			type,
			className: getModifiers(this.baseClass, [style, isWide ? 'wide' : null, isLoading ? 'loading' : null, size])
		};

		if (onClick) {
			atts.onClick = onClick;
		}

		if (isDisabled || isLoading) {
			atts.disabled = true;
		}

		return atts;
	}

	render() {
		const {label, iconPrefix, iconSuffix} = this.props;
		const {isLoading} = this.state;

		return (
			<button {...this.atts}>
				{isLoading && <Loader />}
				<span className={`${this.baseClass}__inner`}>
					<Icon icon={iconPrefix} />
					<Text content={label} />
					<Icon icon={iconSuffix} />
				</span>
			</button>
		);
	}
}

Button.defaultProps = {
	style: null,
	size: null,
	type: 'submit',
	isWide: false,
	isDisabled: false,
	label: '',
	onClick: null,
	iconPrefix: null,
	iconSuffix: null
};

Button.propTypes = {
	style: PropTypes.string,
	size: PropTypes.string,
	type: PropTypes.string.isRequired,
	isWide: PropTypes.bool,
	isDisabled: PropTypes.bool,
	label: PropTypes.string.isRequired,
	onClick: PropTypes.func,
	iconPrefix: PropTypes.any,
	iconSuffix: PropTypes.any
};

export default Button;
